# ---- root/variables.tf

variable "region" {
  type    = string
  default = "ap-south-1"
}
